# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# author: Kris Demuynck (kris.demuynck@gmail.com)
# date: 2023-04-18
# version 2.1
# description: this program calculates, displays and evaluates a decision tree
#              using an improved version of the ID3 algorithm
#              it is writen in Python
# (c) Kris Demuynck

import numpy as np
import pandas as pd
import math
import pydot
from uuid import uuid4
import io
import matplotlib.image as mpimg


# calculates the entropy of a given pandas series
def calculate_entropy(target: pd.Categorical):
    freqs = target.value_counts()/len(target)
    log_freqs = freqs.apply(lambda x: math.log2(x) if (x!=0) else (-10000.0))
    return - (freqs * log_freqs).sum()


# calculates the gini index of a given pandas series
def calculate_gini(target: pd.Categorical):
    freqs = target.value_counts()/len(target)
    return 1 - (freqs ** 2).sum()


# calculates the information gain of a column
# the column is assumed to have a limited number of discrete values
# - column: the column of which the information gain needs to be calculated
# - target: the target column that needs to be predicted
# - method: this is 'entropy' or 'gini'
# returns the calculated information gain
def calculate_information_gain_discrete(column: pd.Series, target: pd.Categorical, method='entropy'):
    method_function = calculate_entropy if method=='entropy' else calculate_gini
    result = method_function(target)
    categories = column.unique()
    n = len(target)
    for category in categories:
        p = (column == category).sum()
        child_target = target[column == category]
        entropy = method_function(child_target)
        result = result - p/n * entropy
    return result


# calculates the information gain of a column, given a certain splitpoint
# the column is assumed to contain numerical values of at least ordinal level
# - column: the column of which the information gain needs to be calculated
# - target: the target column that needs to be predicted
# - split_point: the point where the data needs to be split into 2 categories
# - method: this is 'entropy' or 'gini'
# returns the calculated information gain
def calculate_information_gain_with_split_point(column: pd.Series, target: pd.Categorical, split_point: float, method='entropy'):
    column = pd.cut(column, bins=(-math.inf, split_point, math.inf))
    gain = calculate_information_gain_discrete(column, target, method)
    return gain


# utility method that returns the rounded value of a number with 4 significant digits
round_sig_4 = lambda f: float(('%.' + str(4) + 'e') % f)


# determines where a column has to be split in order to find the optimal split point
# - column: the column that needs to be split
#           the column is assumed to contain numerical values of at least ordinal level
# - target: the target column that needs to be predicted
# - left, right: the interval in which split points should be generated
# - max_splits: the maximum number of split points to be generated
# - method: this is 'entropy' or 'gini'
# returns a list that contains the split points
def generate_splitpoints(column: pd.Series, max_splits):
    left = column.min()
    right = column.max()
    if left == right:
        return []
    uniq = column.unique()
    if len(uniq) > max_splits:
        delta = (right-left) / max_splits
        testpoints = np.arange(left+delta, right, delta)
    else:
        uniq.sort()
        testpoints = []
        for i in range(len(uniq)-1):
            testpoints.append((uniq[i]+uniq[i+1])/2)
    return testpoints


# finds the best point to split the values of a certain column such that the information gain is as high as possible
# - column: the column that needs to be split
#           the column is assumed to contain numerical values of at least ordinal level
# - target: the target column that needs to be predicted
# - method: this is 'entropy' or 'gini'
# returns the optimal split point and the information gain when the data is split at the given point
def find_split_point(column: pd.Series, target: pd.Categorical, method='entropy'):
    max_gain = 0
    split_point = column.min()
    testpoints = generate_splitpoints(column, 31)
    for testpoint in testpoints:
        gain = calculate_information_gain_with_split_point(column, target, testpoint, method)
        if gain > max_gain:
            max_gain = gain
            split_point = testpoint
    return split_point, max_gain


# finds the column with the highest information gain
# - data: the dataframe containing all data (without the target column)
# - target: the target column that needs to be predicted
# - method: this is 'entropy' or 'gini'
# - discrete_columns: a set of column names that contain discrete (nominal) values.
#                     these columns are not split in categories.
# returns the column name, information gain, and optimal split point
def find_column_with_highest_gain(data: pd.DataFrame, target: pd.Categorical, method='entropy', discrete_columns=None):
    if discrete_columns is None:
        discrete_columns = set()
    max_gain = 0
    result = data.columns.tolist()[0]
    best_split_point = pd.NA
    for column in data.columns:
        #print('examining column ', column)
        if column in discrete_columns:
            #print('- column is discrete')
            gain = calculate_information_gain_discrete(data[column], target, method=method)
            split_point = pd.NA
            #print('- gain = ', gain)
        else:
            #print('- column is not discrete')
            split_point, gain = find_split_point(data[column], target, method=method)
            #print('- gain = ', gain)
            #print('- split point = ', split_point)
        if gain > max_gain:
            max_gain = gain
            result = column
            best_split_point = split_point
    return result, max_gain, best_split_point


# determines if a given Series contains only numeric values
# NaN, pd.NA and other missing values are considered non-numeric
def is_numeric(s: pd.Series):
    return not(pd.to_numeric(s, errors='coerce').isnull().any())

# find columns containing discrete values
# max_values gives the maximum number of values that are allowed in a numerical column for it to be considered as discrete
# if a column is not numerical, it is always regarded as discrete
# returns a list of names of all the column containing discrete values
def find_discrete_columns(data: pd.DataFrame, max_values = 4):
    result = [ ]
    for column in data.columns:
        if is_numeric(data[column]):
            number_of_values = len(data[column].unique())
            if (number_of_values <= max_values):
                result = result + [ column ]
        else:
            result = result + [ column ]
    return result


# this class holds the data of a decision tree
# it contains the information of the root node and references to the child nodes
# the following attributes are stored:
# - column: the name of the column that needs to be examined or the value of the result if this is a leaf node
# - target_classes: the names of all columns that are used to base a decision on
# - frequencies: if this is a leaf node, it contains the frequencies of all possible outcomes (indexes in target_classes)
# - children: is a list of all child nodes
# - values: is a list that contains the value of the column for each child node
# - splitpoint: is the place where the column was split if it was non-discrete (None otherwise)
class DecisionTree:
    def __init__(self, column, target_classes):
        self.column = column
        self.target_classes = target_classes
        self.frequencies = [] # only if this is a leaf
        self.children = [] # all child nodes
        self.values = [] # the values of the arrows to the children
        self.splitpoint = None # the splitpoint if the column was not discrete

    def addChild(self, tree):
        self.children.append(tree)

    def addFrequency(self, frequency):
        self.frequencies.append(frequency)

    def addValue(self, value):
        self.values.append(value)

    def isLeaf(self):
        return len(self.children)==0

    def set_splitpoint(self, splitpoint):
        self.splitpoint = splitpoint


# calculates and prints a decision tree for the given data and the given target
# - data: the dataframe containing all data (without the target column)
# - target: the target column that needs to be predicted
# - target_classes: a list of all possible targets
# - method: this is 'entropy' or 'gini'
# - discrete_columns: a set of column names that contain discrete (nominal) values.
#                     these columns are not split in categories.
#                     if None is passed, the algorithm will determine the discrete columns automatically
# - max_values: used to determine the discrete columns if none were given
# - max_depth: maximum depth for the tree
# - depth: used internally for the recursive operation, don't use this
def id3_improved_helper(data: pd.DataFrame, target: pd.Categorical, target_classes, method='entropy', discrete_columns=None, max_values=4, max_depth=math.inf, depth=0):
    if discrete_columns is None:
        discrete_columns = find_discrete_columns(data, max_values)
    # determine if recursion can stop (target contains only one value)
    unique_values = target.unique()
    if len(unique_values) == 1:
        tree = DecisionTree(unique_values[0], target_classes)
        for target_class in target_classes:
            tree.addFrequency(len(target) if (target_class == unique_values[0]) else 0)
        return tree
    if depth >= max_depth or depth >= len(data.columns):
        freqs = target.value_counts()
        tree = DecisionTree("", target_classes)
        max_freq = 0
        mode = ""
        for target_class in target_classes:
            freq = freqs[target_class]
            if  freq > max_freq:
                max_freq = freq
                mode = target_class
            tree.addFrequency(freq)
        tree.column = mode
        return tree
    column_name, gain, split_point = find_column_with_highest_gain(data, target, method, discrete_columns)
    tree = DecisionTree(column_name, target_classes)
    if not pd.isna(split_point):
        tree.set_splitpoint(split_point)
        split_point = round_sig_4(split_point)
        split_column = pd.cut(data[column_name], bins=(-math.inf, split_point, math.inf))
        split_column = split_column.cat.rename_categories(['<= ' + str(split_point), '> ' + str(split_point)])
    else:
        split_column = data[column_name]
    # print node-name
    categories = split_column.unique()
    for category in categories:
        # calculate the childtable for this value
        child_table = data.loc[split_column == category]
        child_target = target[split_column == category]
        # call ID3 recursively
        child = id3_improved_helper(child_table, child_target, target_classes, method, discrete_columns, max_values, max_depth, depth + 1)
        tree.addValue(category)
        tree.addChild(child)
    return tree


# calculates and prints a decision tree for the given data and the given target
# - data: the dataframe containing all data (without the target column)
# - target: the target column that needs to be predicted
# - method: this is 'entropy' or 'gini'
# - discrete_columns: a set of column names that contain discrete (nominal) values.
#                     these columns are not split in categories.
#                     if None is passed, the algorithm will determine the discrete columns automatically
# - max_values: used to determine the discrete columns if none were given
# - max_depth: maximum depth for the tree
def id3_improved(data: pd.DataFrame, target: pd.Series, method='entropy', discrete_columns=None, max_values=4, max_depth=math.inf):
    target = pd.Categorical(target.apply(str))
    target_classes = target.categories.tolist()
    return id3_improved_helper(data, target, target_classes, method, discrete_columns, max_values, max_depth)


# creates a string of all the frequencies in a node
# - tree: the node
# - as_dict: indicates if teh result should be returned as a dictionary
# returns a string holding all frequencies per possible value
# if there is only one frequency non-zero and as_dict is False, an empty string is returned
def get_frequencies(tree: DecisionTree, as_dict=False):
    if as_dict:
        result = {}
        for i in range(len(tree.target_classes)):
            result[tree.target_classes[i]] = tree.frequencies[i]
        return result
    else:
        count = 0
        for freq in tree.frequencies:
            if freq != 0:
                count += 1
        if count == 1:
            return ''
        else:
            return str(tree.frequencies)


# prints the decision tree as text
# - tree: the tree that needs to be printed
# - depth: the amount of tabs (\t) that need to be inserted before each line
# - show_freqs: a boolean indicating if the leaf nodes should show the frequencies for all possible values
def print_decision_tree(tree: DecisionTree, show_freqs: bool=False, depth=0):
    if tree.isLeaf():
        if show_freqs:
            print('\t' * depth + tree.column + ' ' + get_frequencies(tree))
        else:
            print('\t' * depth + tree.column)
    else:
        print('\t'*depth + tree.column + ':')
        for i in range(len(tree.children)):
            child = tree.children[i]
            value = tree.values[i]
            print('\t'*(depth+1) + str(value) + ':')
            print_decision_tree(child, show_freqs, depth+2)


# helper function to make a plot of a tree in a graph
# - tree: the tree (or subtree) to be plotted
# - graph: the graph onto which the tree must be plotted
# - show_freqs: a boolean indicating if the leaf nodes should show the frequencies for all possible values
def plot_decision_tree_helper(tree: DecisionTree, graph: pydot.Graph, show_freqs: bool):
    if tree.isLeaf():
        if show_freqs:
            leaf_node = pydot.Node(name=str(uuid4()), label=tree.column + '\n' + get_frequencies(tree))
        else:
            leaf_node = pydot.Node(name=str(uuid4()), label=tree.column)
        graph.add_node(leaf_node)
        return leaf_node.get_name()
    else:
        parent_node = pydot.Node(name=str(uuid4()), label=tree.column, shape='box')
        graph.add_node(parent_node)
        for i in range(len(tree.children)):
            child = tree.children[i]
            value = tree.values[i]
            child_node_name = plot_decision_tree_helper(child, graph, show_freqs)
            edge = pydot.Edge(parent_node.get_name(), child_node_name, label=value)
            graph.add_edge(edge)
        return parent_node.get_name()


# plots a decision tree as a dot graph
# if an ax object is supplied the tree is plotted on that
# if a filemane is supplied the tree is saved to disk
# - tree: the tree to be plotted
# - ax: the ax-object onto which the tree is drawn (if applicable)
# - filename: the name of the file to which the image should be written
# - show_freqs: a boolean indicating if the leaf nodes should show the frequencies for all possible values
def plot_decision_tree(tree: DecisionTree, ax=None, filename:str=None, show_freqs:bool=False):
    graph = pydot.Dot('Decision tree', graph_type='digraph')
    plot_decision_tree_helper(tree, graph, show_freqs)
    if ax is not None:
        png_str = graph.create_png(prog='dot')
        sio = io.BytesIO()
        sio.write(png_str)
        sio.seek(0)
        img = mpimg.imread(sio)
        ax.axis('off')
        ax.imshow(img)
    if filename is not None:
        parts = filename.split('.')
        extension = parts[-1]
        exec('graph.write_' + extension + '("' + filename + '")')


# returns code that will evaluate the tree for a given input
# the code will examine a variable called 'input' (of type list)
# and returns the decision in a variable called 'result'
# if no rule is found for the input, the result will be 'None'
# - tree: the tree to be transformed into code
# - show_freqs: a boolean indicating if the leaf nodes should return the frequencies for all possible values
# - depth: the amount of tabs (\t) that need to be inserted before each line
def code_decision_tree(tree: DecisionTree, inputname:str='input', resultname:str='result', show_freqs:bool=False, depth=0):
    generated_code = ''
    if tree.isLeaf():
        generated_code += ('\t' * depth) + resultname + ' = '
        if show_freqs:
             generated_code += str(tree.frequencies) + '\n'
        else:
            generated_code += "'" + str(tree.column) + "'\n"
        return generated_code
    else:
        for i in range(len(tree.children)):
            if i == 0:
                generated_code += ('\t' * depth) + 'if ' + inputname + "['" + str(tree.column) + "'] "
            else:
                generated_code += ('\t' * depth) + 'elif ' + inputname + "['" + str(tree.column) + "'] "
            if tree.splitpoint is None:
                generated_code += '== '
            child = tree.children[i]
            value = tree.values[i]
            if tree.splitpoint is None:
                value = "'" + str(value) + "'"
            generated_code += str(value) + ':\n'
            generated_code += code_decision_tree(child, inputname, resultname, show_freqs, depth+1)
        generated_code += ('\t' * depth) + 'else:\n' + '\t' * (depth + 1) + resultname + ' = '
        if show_freqs:
            generated_code += '[ 1 ] * ' + str(len(tree.target_classes)) + '\n'
        else:
            generated_code += 'None\n'
        return generated_code


# evaluates a tree for a given input
# - tree: the decision tree to be used
# - input: a dict, Series containing a value for each possible input
# - show_freqs: a boolean indicating if the leaf nodes should return the frequencies for all possible values
# returns the decision of the tree or the frequencies
def evaluate_tree(tree:DecisionTree, input, show_freqs:bool=False):
    if tree.isLeaf():
        if show_freqs:
            return get_frequencies(tree, as_dict=True)
        else:
            return tree.column
    else:
        for i in range(len(tree.children)):
            child = tree.children[i]
            value = tree.values[i]
            if input[tree.column] == value:
                return evaluate_tree(child, input, show_freqs)
            elif (tree.splitpoint is not None) and (value[0]=='<') and (input[tree.column] <= tree.splitpoint):
                return evaluate_tree(child, input, show_freqs)
            elif (tree.splitpoint is not None) and (value[0]=='>') and (input[tree.column] > tree.splitpoint):
                return evaluate_tree(child, input, show_freqs)
        if show_freqs:
            result = {}
            for i in range(len(tree.target_classes)):
                result[tree.target_classes[i]] = 1
            return result
        else:
            return None


# predicts the outcome of a decision tree for all rows in a dataframe
# - tree: the decision tree to be used
# - values: a dataframe that contains different cases to be evaluated in its columns
# - show_freqs: a boolean indicating if the leaf nodes should return the frequencies for all possible values
# returns a list of all decisions of the tree for each row in the dataframe (None if no rule matches the input)
def predict(tree: DecisionTree, values: pd.DataFrame, show_freqs:bool=False):
    result = []
    for row_index in range(len(values)):
        row = values.iloc[row_index]
        new_row = evaluate_tree(tree, row, show_freqs)
        result.append(new_row)
    return result
