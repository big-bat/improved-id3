### Description
This project contains an improved version of the ID3 algorithm.
It can handle a mix of discrete and non-discrete variables.

The library can be found in the file id3_improved.py

The id3-improved_example.ipynb file contains an example to show how to use the library.

Contact the author if you have any questions or remarks.

### Copyright 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

**author**: Kris Demuynck (kris.demuynck@gmail.com)

**date**: 2023-04-18

**version** 2.1

**description**: this program calculates and displays a decision tree
             using an improved version of the ID3 algorithm
             it is writen in Python

